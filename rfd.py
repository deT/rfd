import feedparser
import subprocess
import time

def fetch_rss_feed(url):
  # Use feedparser to fetch the RSS feed
  feed = feedparser.parse(url)
 
  # Iterate through the entries in the feed
  for entry in feed.entries:
    # Check if the entry is a new entry
    if entry.title not in previous_entries:
      # Send a notification for the new entry
      send_notification(entry.title, entry.summary)
      # Add the entry to the list of previous entries
      previous_entries.append(entry.title)

def send_notification(title, message):
  # Use subprocess to run the dunstify command
  subprocess.run(['dunstify', '-a', 'rss-feed', title, message])

# The list of previous entries
previous_entries = []

# The URL of the RSS feed
rss_feed_url = 'http://feeds.bbci.co.uk/news/rss.xml'

# The delay between each iteration of the loop (in seconds)
delay = 60

# Run the program in a loop
while True:
  # Fetch the RSS feed
  fetch_rss_feed(rss_feed_url)
  # Sleep for the specified delay
  time.sleep(delay)
